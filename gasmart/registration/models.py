from django.db import models
from django.http import HttpResponse
from mongoengine import Document, EmbeddedDocument, fields, ValidationError
from django.contrib.auth import password_validation, hashers
from django.conf import settings
from rest_framework import status
from django.db.models.options import Options


# class UserNames(EmbeddedDocument):
#     firstName = fields.StringField(required=True)
#     lastName = fields.StringField(required=True)
#
# class Address(EmbeddedDocument):
#     address = fields.StringField(required=True)
#     # email = fields.EmailField()
#     addressGeo = fields.GeoPointField

# class Security(EmbeddedDocument):
#     password = fields.StringField(required=True)




class Signup(Document):
    firstName = fields.StringField(required=True)
    lastName = fields.StringField(required=True)
    address = fields.StringField()
    addressGeo = fields.GeoPointField()
    dob = fields.DateTimeField(help_text='dd/mm/yyyy')
    gender = fields.StringField(required=True)
    password = fields.StringField(required=True)
    phone_number = fields.StringField(required=True)
    email = fields.EmailField(required=True)
    class Meta:
        fields = '__all__'

    def clean(self):
        if  password_validation.validate_password(self.password,password_validators=None) is not None:
            raise fields.ValidationError('Password issues')
        else:
            self.password = hashers.make_password(self.password)
