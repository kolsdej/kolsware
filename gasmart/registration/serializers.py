from rest_framework_mongoengine import serializers
from . import models

class SignupSerializer(serializers.DocumentSerializer):
    class Meta:
        model = models.Signup
        fields = '__all__'

        # related_model_validations = {'password': models.Signup}

