import django
import json
from mongoengine import connect
from rest_framework_mongoengine import viewsets
from django.core import serializers as serial
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import list_route, detail_route
from . import serializers
from . import models
from django.contrib.auth import hashers

class SignupViewSet(viewsets.ModelViewSet):
    lookup_field = 'email'
    serializer_class = serializers.SignupSerializer
    # queryset = models.Signup.objects.all()

    def get_queryset(self):
        try:
            return models.Signup.objects.all()
        except :
            return Response(status=status.HTTP_404_NOT_FOUND)

    @list_route(methods=['post'],url_path='userLogIn')
    def log_in_users(self, request):
        serializer = serializers.SignupSerializer(request.data)
        print(serializer.data)
        user = models.Signup.objects.get(email=serializer.data['email'])
        print(user)
        if user.password is not None:
            if hashers.check_password(serializer.data['password'],user.password):
                json_serializer = serial.get_serializer("json")()
                response = serial.serialize(user,'json')
                return Response(data=response,status=status.HTTP_200_OK, content_type='application/json')
        return Response(data= json_error_response('G01', 'Failed to login'),status=status.HTTP_400_BAD_REQUEST, content_type='application/json')

def json_login_successful_response( user):
    response_data = {}
    response_data['firstName'] = user.name.firstName
    response_data['lastName'] = user.name.lastName
    return response_data
def json_error_response(errorCode, errorMessage):
    response_data = {}
    response_data['code'] = errorCode
    response_data['description'] =  errorMessage
    return  response_data

